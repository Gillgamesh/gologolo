const mongoose = require('mongoose');


const baseCharacteristics = {
    borderRadius: { type: Number},
    borderThickness: { type: Number},
    borderColor: String,
    backgroundColor: String,
    padding: { type: Number},
}

const CoordinateSchema = new mongoose.Schema(
    {
        posX: Number,
        posY: Number,
        positionStyle: String
    }
)
const ImageSchema = new mongoose.Schema(
    {
        ...baseCharacteristics,
        url: String,
        position: CoordinateSchema,
        priority: Number,
        scale: { type: Number},
    }
)

const TextSchema = new mongoose.Schema(
    {
        ...baseCharacteristics,
        text: String,
        position: CoordinateSchema,
        color: String,
        fontSize: Number,
        priority: Number
    }
)

const LogoSchema = new mongoose.Schema({
    ...baseCharacteristics,
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User"
    },
    lastUpdate: { type: Date, default: Date.now },
    images: [ImageSchema],
    text: [TextSchema],
    margin: { type: Number},
    width: { type: Number},
    height: { type: Number},
});
console.log(LogoSchema);

module.exports = mongoose.model('Logo', LogoSchema);
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
    displayName: String,
    googleUserId: String,
    isAdmin: Boolean,
    // logos save userId, we then query
});

module.exports = mongoose.model('User', UserSchema);
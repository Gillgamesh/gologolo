var GraphQLSchema = require('graphql').GraphQLSchema;
var GraphQLList = require('graphql').GraphQLList;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLInputObjectType = require('graphql').GraphQLInputObjectType;
var GraphQLNonNull = require('graphql').GraphQLNonNull;
var LogoModel = require('../models/Logo');
var GraphQLID = require('graphql').GraphQLID;
var GraphQLString = require('graphql').GraphQLString;
var GraphQLInt = require('graphql').GraphQLInt;
var GraphQLFloat = require('graphql').GraphQLFloat;
var GraphQLDate = require('graphql-date');

var LogoModel = require('../models/Logo');


const nonNullifier = (fields) => (
    Object.fromEntries(
        Object.entries(fields).map(
            ([k, v]) => [
                k,
                {
                    type: new GraphQLNonNull(v.type)
                }
            ]
        )
    )
);

let noId = (fields) => {
    let {_id, ...rest} = fields;
    return rest;
}

// noId = (fields) => noId(nonNullifier(fields));

const positionScalarFields = {
    posX: {
        type: GraphQLInt
    },
    posY: {
        type: GraphQLInt
    },
}

const positionTypeConfig = {
    name: 'Position', 
    fields: () => ({
        ...positionScalarFields,
        _id: {
            type: GraphQLString
        },
    })
}

const positionInputTypeConfig = {
    name: 'PositionInput',
    fields: () => nonNullifier({
        ...positionScalarFields
    })
}

const positionType = new GraphQLObjectType(positionTypeConfig);
const positionInputType = new GraphQLInputObjectType(positionInputTypeConfig);

const baseCharacteristics = {
    borderRadius: { type: GraphQLInt },
    borderThickness: { type: GraphQLInt },
    padding: { type: GraphQLInt },
    borderColor: { type: GraphQLString },
    backgroundColor: { type: GraphQLString },
}

const textScalarFields = {
    ...baseCharacteristics,
    text: {
        type: GraphQLString
    },
    color: {
        type: GraphQLString
    },
    fontSize: {
        type: GraphQLInt
    },
    priority: {
        type: GraphQLInt
    },
};

const textTypeConfig = {
    name: 'Text',
    fields: () => ({
        ...textScalarFields,
        _id: {
            type: GraphQLString
        },
        position: {
            type: positionType 
        }
    })
} 

const textInputTypeConfig = {
    name: 'TextInput',
    fields: () => nonNullifier({
        ...noId(textScalarFields),
        position: {
            type: positionInputType 
        }
    })
} 

const textType = new GraphQLObjectType(textTypeConfig);
const textInputType = new GraphQLInputObjectType(textInputTypeConfig);

const imageScalarFields = {
    ...baseCharacteristics,
    url: {
        type: GraphQLString
    },
    priority: {
        type: GraphQLInt
    },
    scale: {
        type: GraphQLInt
    },
};

const imageTypeConfig = {
    name: 'Image', 
    fields: () => ({
        ...imageScalarFields,
        _id: {
            type: GraphQLString
        },
        position: {
            type: positionType 
        }
    })
}

const imageInputTypeConfig = {
    name: 'ImageInput', 
    fields: () => nonNullifier({
        ...noId(imageScalarFields),
        position: {
            type: positionInputType 
        }
    })
}

const imageType = new GraphQLObjectType(imageTypeConfig);
const imageInputType = new GraphQLInputObjectType(imageInputTypeConfig);

const logoScalars = {
    ...baseCharacteristics,
    margin: {
        type: GraphQLInt
    },
    width: {
        type: GraphQLInt
    },
    height: {
        type: GraphQLInt
    },
}
const logoTypeConfig = {
    name: 'Logo',
    fields: () => ({
        ...logoScalars,
        images: {
            type: new GraphQLList(imageType)
        },
        text: {
            type: new GraphQLList(textType)
        },
        _id: {
            type: GraphQLString
        },
        lastUpdate: {
            type: GraphQLDate
        },
        userId: {
            type: GraphQLInt
        },

    })
};

const logoInputTypeConfig = {
    name: 'LogoInput',
    fields: () => nonNullifier({
        ...noId(logoScalars),
        images: {
            type: new GraphQLList(imageInputType)
        },
        text: {
            type: new GraphQLList(textInputType)
        },
    })
};



const logoType = new GraphQLObjectType(logoTypeConfig);
const logoInputType = new GraphQLInputObjectType(logoInputTypeConfig)

const userTypeConfig = {
    name: 'User',
    fields: () => ({
        _id: {
            type: GraphQLString
        },
        displayName: {
            type: GraphQLString
        },
        googleUserId: {
            type: GraphQLString
        },
        logos: {
            type: new GraphQLList(logoType),
            resolve: (parent, params, context) => {
                return LogoModel.find({userId: parent._id}).exec();
            }
        }

    })
};

const userType = new GraphQLObjectType(userTypeConfig);

exports.positionTypeConfig = positionTypeConfig;
exports.positionType= positionType;

exports.textTypeConfig = textTypeConfig;
exports.textType= textType;

exports.imageTypeConfig = imageTypeConfig;
exports.imageType= imageType;

exports.logoTypeConfig = logoTypeConfig;
exports.logoType = logoType;
exports.logoInputTypeConfig = logoInputTypeConfig;
exports.logoInputType = logoInputType;

exports.userTypeConfig = userTypeConfig;
exports.userType = userType;

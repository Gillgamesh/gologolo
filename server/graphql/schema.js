var GraphQLSchema = require('graphql').GraphQLSchema;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLList = require('graphql').GraphQLList;
var GraphQLObjectType = require('graphql').GraphQLObjectType;
var GraphQLNonNull = require('graphql').GraphQLNonNull;
var LogoModel = require('../models/Logo');
var logoTypeConfig = require('./types').logoTypeConfig;
var logoType = require('./types').logoType;
var logoInputType = require('./types').logoInputType;
var userType = require('./types').userType;

var GraphQLID = require('graphql').GraphQLID;
var GraphQLString = require('graphql').GraphQLString;
var GraphQLInt = require('graphql').GraphQLInt;
var GraphQLDate = require('graphql-date');


var queryType = new GraphQLObjectType({
    name: 'Query',
    fields: function () {
        return {
            users: {
                type: new GraphQLList(userType),
                resolve: (root, params, context) => {
                    const users = UserModel.find().exec();
                    if (!users) {
                        throw new Error('Error')
                    }
                    return users;
                }
            },
            viewer: {
                type: userType,
                resolve: (root, params, context) => {
                    return context.user;
                }
            },
            logos: {
                type: new GraphQLList(logoType),
                resolve: function (root, params, context) {
                    const logos = LogoModel.find().exec();
                    if (!logos) {
                        throw new Error('Error')
                    }
                    return logos;
                }
            },
            viewerLogos: {
                type: new GraphQLList(logoType),
                resolve: function (root, params, context) {
                    const logos = LogoModel.find({userId: context.user._id}).exec();
                    if (!logos) {
                        throw new Error('Error')
                    }
                    return logos;
                }
            },
            logo: {
                type: logoType,
                args: {
                    id: {
                        name: '_id',
                        type: GraphQLString
                    }
                },
                resolve: function (root, params) {
                    const logoDetails = LogoModel.findById(params.id).exec()
                    if (!logoDetails) {
                        throw new Error('Error')
                    }
                    return logoDetails
                }
            }
        }
    }
});

var mutation = new GraphQLObjectType({
    name: 'Mutation',
    fields: function () {
        return {
            addLogo: {
                type: logoType,
                args: {
                    logo: {
                        name: 'logo',
                        type: logoInputType
                    }
                },
                resolve: function (root, params, context) {
                    let fields = {
                        ...params.logo,
                        userId: context.user._id,
                        lastUpdate: new Date()
                    }
                    const logoModel = new LogoModel(fields);
                    logoModel.save().then(
                        () => {
                            // context.user.logos.push(logoModel._id);
                            // context.user.save();
                        }
                    );
                    return logoModel;
                }
            },
            updateLogo: {
                type: logoType,
                args: {
                    id: {
                        name: 'id',
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    logo: {
                        name: 'logo',
                        type: logoInputType
                    }
                },
                resolve(root, params, context) {
                    return LogoModel.findOneAndUpdate(
                        {
                            _id: params.id,
                            userId: context.user._id
                        },
                        { ...params.logo, lastUpdate: new Date() },
                        function (err) {
                            if (err) return next(err);
                    });
                }
            },
            removeLogo: {
                type: logoType,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(root, params, context) {
                    const remLogo = LogoModel.findOneAndRemove(
                        {
                            _id: params.id,
                            userId: context.user._id
                        }
                    ).exec();
                    if (!remLogo) {
                        throw new Error('Error')
                    }
                    return remLogo;
                }
            }
        }
    }
});

module.exports = new GraphQLSchema({ query: queryType, mutation: mutation });
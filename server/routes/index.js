var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.redirect(process.env.GOLO_CLIENT_ADDR)
});

module.exports = router;

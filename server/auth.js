var passport = require('passport');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var GoogleStrategy = require('passport-google-oauth20').Strategy;
var UserModel = require('./models/User');


const jwtOptions = {
    jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
    secretOrKey: process.env.GOLO_SECRET,
}

const jwtStrategy = new JwtStrategy(jwtOptions, (payload, done) => {
    let id = payload.sub;
    UserModel.findById(id, (err, user) => {
        if (err)
            done(err, null);
        else
            done(null, user);

    })
});


const googleStrategy = new GoogleStrategy(
    {
        clientID: process.env.GOLO_GOOGLE_ID,
        clientSecret: process.env.GOLO_GOOGLE_SECRET,
        callbackURL: `${process.env.GOLO_SERVER_ADDR}/auth/google/callback`
    },
    async function (accessToken, refreshToken, profile, done) {
        let id = profile.id;
        if (!id) {
            done(err, user);
        } else
        UserModel.findOne({ googleUserId: id }, (err, user) => {
            if (err)
                done(err, user);
            else {
                if (user)
                    done(null, user);
                else {
                    user = new UserModel({
                        googleUserId: id,
                        isAdmin: false,
                        displayName: profile.displayName,
                    })
                    user.save((error)=> {
                        if (err)
                            done(err, null);
                        else
                            done(null, user);
                    })
                }
            }
        }
    );
    }
);

// we want to define an object with initialize() and auth() functions that automatically perform certain tasks for us
// since we are setting everything up in a seperate file, we'll create everything through a function that is exported

module.exports = () => {
    passport.use(jwtStrategy);
    passport.use(googleStrategy);

    // serialize and deserialize user for the google strategy
    passport.serializeUser(function(user, done) {
        done(null, user._id);
      });

      passport.deserializeUser(function(id, done) {
          userDocument = UserModel.findById(id, (err, user) => {
              if (err)
                  done(err, null);
              else
                  done(null, user);

          })
      });

    return {
        initialize: () => passport.initialize(),
        useSession: () => passport.session(),
        authenticate: () => passport.authenticate('jwt', {session: false}),
        authGoogle: () => passport.authenticate('google', {prompt: 'select_account', scope: ['profile', 'email']}),
        passport: passport
    };
}

var createError = require('http-errors');
var express = require('express');
var jwt = require('jsonwebtoken');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var mongoose = require('mongoose');
var graphqlHTTP = require('express-graphql');
var schema = require('./graphql/schema');
var session = require('express-session');
var authSetup = require('./auth');
var cors = require("cors");


const auth = authSetup();

const corsSettings = {
    origin: process.env.GOLO_CLIENT_ADDR,
    credentials: true,
    methods: ['GET', 'PUT', 'POST']
}

mongoose.connect(process.env.GOLO_MONGO_ADDR, { promiseLibrary: require('bluebird'), useNewUrlParser: true })
    .then(() => console.log('connection successful'))
    .catch((err) => console.error(err));

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();
app.use(session({secret: process.env.GOLO_SECRET}));

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')))


app.use(auth.initialize());
app.use(auth.useSession());

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use('*', cors());

app.get('/auth/google', auth.authGoogle());

app.get('/auth/google/callback',
    cors(),
    auth.passport.authenticate('google'),
    (req, res) => {
        return res.redirect(process.env.GOLO_CLIENT_ADDR);
    }
)

function checkGoogleAuth(req, res, next) {
    if (req.user)
        next();
    else
        res.status(401).send({
            message: 'Not authenticated!'
        });
}

app.get('/auth/getToken', cors(corsSettings), checkGoogleAuth, (req, res) => {
    // get the user id, create an access token:
    // set it to expire in 24 hours, because I don't want to deal with refreshing
    // we are already able to pseudo-refresh from this method anywaysm since
    // cookied sessions are maintained
    // The intent is that this token will be stored in sessionStorage
    let id = req.user._id;
    let token = jwt.sign(
        {
            iat: Math.floor(Date.now() / 1000) + 60*60*24,
            sub: id
        },
        process.env.GOLO_SECRET
    )
    return res.json({accessToken: token, user: req.user});
});

app.get('/auth/clearSession', cors(corsSettings), (req, res) => {
    if (req.user && req.logout) {
        req.logout();
        return res.json({success: true});
    }
    return res.json({success: false});
});

app.use('/graphql', auth.authenticate(), graphqlHTTP({
    schema: schema,
    rootValue: global,
    graphiql: true,
}));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;

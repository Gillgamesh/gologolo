# Gologolo 2, 2 Electric Boogaloo, 2: Electric Boogaloo

## Explanation:

This uses sessions to store Google OAuth tokens and then serves the server's own JWTs based off that. You need an internet connection to test this; without signin with google this will not work. 

You must define the following environment variables: `GOLO_CLIENT_ADDR`, `GOLO_SERVER_ADDR`, `GOLO_MONGO_ADDR`, `GOLO_SECRET`, `GOLO_GOOGLE_ID`, `GOLO_GOOGLE_SECRET`

## Usage:

* You can log out from the top-right top bar. You cannot access the page without logging in via Google. There's also a mongo server that needs to be set up on the defalt port.
* Logos are account specific; you can try that out by using two Gmails. 
* You can use either the home page or the drawer to access your logos/create a new one. You can delete them or download them as a PNG from the view screen. If you press edit, you'll see that the workspace area that was previously immutable will now let you move around your text/iamges thanks to react-draggable. 
* Settings are controlled accordian style. 
* ORDERING in both the accordians and the render order is determined by HIGHEST priority. I.e. the highest priority element will render on top of other elements, and will be moved to the top of the layers on the left.

## Queries:

You can find them at `{server}/graphql` via the schema. There's also the examples I used for testing at the bottom of `queries.txt`. In order to run them from there, you need to provide valid auth tokens (you can sign in and grab one from sessionStorage, if you wish). I used a chrome extension that lets me modify request headers, setting "Authorization" to "Bearer `{token}`". Without those, the only way to test is directly through the client. All of the queries except for `allLogos` and `logo` are viewer-specific anyways (i.e. they will figure out the user's ID from the token in the headers, won't be asked for in the query itself). 

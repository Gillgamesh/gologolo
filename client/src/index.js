import React from 'react';
import ReactDOM from 'react-dom';

import ApolloClient from 'apollo-client';
import { ApolloProvider } from 'react-apollo';
import {ApolloLink} from 'apollo-link';
import {onError} from 'apollo-link-error';
import {createHttpLink} from 'apollo-link-http';
import { setContext } from 'apollo-link-context';
import {InMemoryCache} from 'apollo-cache-inmemory';



import { BrowserRouter as Router, Route } from 'react-router-dom';
import '../node_modules/bootstrap/dist/css/bootstrap.min.css';
import * as serviceWorker from './serviceWorker';

// THESE ARE OUR REACT SCREENS, WHICH WE WILL ROUTE HERE
import HomeScreen from './components/HomeScreen';
import EditLogoScreen from './components/EditLogoScreen';
import CreateLogoScreen from './components/CreateLogoScreen';
import ViewLogoScreen from './components/ViewLogoScreen';
import Navigation from './components/Navigation/Navigation';

import { createMuiTheme } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';
import green from '@material-ui/core/colors/green';

import {ThemeProvider as MuiThemeProvider} from '@material-ui/core/styles';
import CssBaseline from '@material-ui/core/CssBaseline';
import { makeStyles } from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';

import {getLocalAccessToken, isTokenValid, isLoggedIn} from './auth';

const httpLink = createHttpLink({
    uri: `${process.env.REACT_APP_GOLO_SERVER_ADDR}/graphql`,
});

const authLink = setContext(
    ( _, { headers }) => {
        const token = getLocalAccessToken();
        if (isLoggedIn()) {
            if (isTokenValid(getLocalAccessToken() || '')) {
                return {
                    headers: {
                        ...headers,
                        Authorization: token ? `Bearer ${token}` : ""
                    },
                };
            }
            /* todo - figure out how to do this properly */
            return headers;
        }
        else {
            fetch(`${process.env.REACT_APP_GOLO_SERVER_ADDR}/auth/getToken`, {credentials: 'include'}).then(
                (res) => {
                    if (res.status !== 200)
                        window.location.href = `${process.env.REACT_APP_GOLO_SERVER_ADDR}/auth/google`
                    else
                        return res.json();
                }
            ).then((res) => {
                if (res)
                    sessionStorage.setItem('accessToken', res.accessToken)
            })
        }
        return headers;
    }
);
const errorLink = onError(({ graphQLErrors, networkError }) => {
    console.log('graphQLErrors', graphQLErrors)
    console.log('networkError', networkError)
});

const link = ApolloLink.from([
    authLink,
    errorLink,
    httpLink,
])

const client = new ApolloClient(
    { link: link, cache: new InMemoryCache() }
);

const theme = createMuiTheme({
    palette: {
        secondary:red,
        type: 'light'
    },
})
const useStyles = makeStyles(theme => ({
    container: {
        marginTop: theme.spacing(12),
    },
}));

const App = () => {
    const muiClasses = useStyles();
    return (
    <ApolloProvider client={client}>
        <MuiThemeProvider theme={theme}>
            <CssBaseline/>
            <Router>
                <div>
                    <Navigation/>
                    <Container className={muiClasses.container}>
                            <Route exact path='/' component={HomeScreen} />
                            <Route path='/edit/:id' component={EditLogoScreen} />
                            <Route path='/create' component={CreateLogoScreen} />
                            <Route path='/view/:id' component={ViewLogoScreen} />
                    </Container>
                </div>
            </Router>
        </MuiThemeProvider>
    </ApolloProvider>
    )
}

ReactDOM.render(
    <App/>,
    document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://create-react-app.dev/docs/making-a-progressive-web-app/
serviceWorker.unregister();

import gql from 'graphql-tag';

const POSITION_FRAGMENTS = gql`
fragment PositionAll on Position {
    posX
    posY
}
`
const TEXT_FRAGMENTS = gql`
fragment TextAll on Text {
    _id
    text
    position {
    ...PositionAll
    }
    fontSize
    borderRadius
    borderThickness
    padding
    borderColor
    backgroundColor
    color
    priority
}
`

const IMAGE_FRAGMENTS = gql`
fragment ImageAll on Image {
    _id
    url
    position {
    ...PositionAll
    }
    borderRadius
    borderThickness
    padding
    borderColor
    backgroundColor
    scale
    priority
}
`

const LOGO_FRAGMENTS = gql`
fragment LogoAll on Logo {
    _id
    padding
    borderColor
    backgroundColor
    borderRadius
    borderThickness
    margin
    lastUpdate
    width
    height
    text {
    ...TextAll
    }
    images {
    ...ImageAll
    }
}
    ${TEXT_FRAGMENTS}
    ${IMAGE_FRAGMENTS}
    ${POSITION_FRAGMENTS}
`

const USER_FRAGMENTS = gql`
fragment UserAll on User {
    id: _id
    googleUserId
    isAdmin
    logos {
    ...LogoAll
    }
}
`
export const GET_LOGO = gql`
    query logo($id: String) {
        logo(id: $id) {
            _id
            id: _id
            ...LogoAll
        }
    }
    ${LOGO_FRAGMENTS}
`;

export const GET_LOGOS = gql`
  {
    viewerLogos {
      _id
      id: _id
      lastUpdate
      text {
        _id
        text
      }
    }
  }
`;

export const ADD_LOGO = gql`
    mutation AddLogo(
        $logo: LogoInput! 
        ) {
        addLogo(
            logo: $logo
            ) {
            _id
        }
    }
`;

export const UPDATE_LOGO = gql`
    mutation updateLogo(
        $_id: String!,
        $logo: LogoInput!
        ) {
            updateLogo(
                id: $_id,
                logo: $logo
                ) {
                    lastUpdate
                }
        }
`;

export const DELETE_LOGO = gql`
  mutation removeLogo($id: String!) {
    removeLogo(id:$id) {
      _id
    }
  }
`;

export const VIEWER_QUERY = gql`
{
    viewer {
        id: _id
        _id
        displayName
    }
}
`
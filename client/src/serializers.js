import {DEFAULT_TEXT, DEFAULT_IMAGE} from './constants';
import {v4 as uuidv4} from 'uuid'; 

export const sortElementsByPriority = (elements) => {
    /*
    Given a list of elements with a priority property,
    returns a copy that sorts by that priority
    */
    let copy = [...elements];
    copy.sort((a, b) => a.priority - b.priority);
    return copy;
}

export const serializeLogo = (logo) => {
    //given a logo from a graphql query, returns a COPY with a new elements field,
    //which contains priority-ordered images/logos list of ids
    let logoCopy = {...logo};
    logoCopy.text = logo.text.map((text)=> {
        return [text._id, text];
    });
    logoCopy.text = Object.fromEntries(logoCopy.text);
    let textIds = Object.values(logoCopy.text).map((text)=> ({
        type: "text",
        _id: text._id,
        priority: text.priority
    }));

    logoCopy.images = logo.images.map((image)=> {
        return [image._id, image];
    });
    logoCopy.images = Object.fromEntries(logoCopy.images);
    let imageIds = Object.values(logoCopy.images).map((image)=> ({
        type: "images",
        _id: image._id,
        priority: image.priority
    }));
    let sortedElements = sortElementsByPriority([...imageIds, ...textIds]);
    logoCopy.elements = sortedElements;
    return logoCopy;
}

export const withUpdatedText = (logo, text) => {
    //given a logo and an updated version of a particular text, returns a new logo
    //with that element replaced with "text"
    //also serves as an append, by updating sortedElements
    let updatedText = {...logo.text};
    updatedText[text._id] = text;
    // remove the element by only keeping the ones with different ids
    let updatedElements = [
        ...logo.elements.filter(elem=>elem._id != text._id), 
        {type: "text", _id: text._id, priority: text.priority}
    ];
    updatedElements = sortElementsByPriority(updatedElements);
    return {...logo, text: updatedText, elements: updatedElements};
}

export const withUpdatedImage = (logo, image) => {
    //given a logo and an updated version of a particular image, returns a new logo
    //with that element replaced with image
    //also serves as an append, by updating sortedElements
    let updatedImages = {...logo.images};
    updatedImages[image._id] = image;
    // remove the element by only keeping the ones with different ids
    let updatedElements = [
        ...logo.elements.filter(elem=>elem._id != image._id), 
        {type: "images", _id: image._id, priority: image.priority}
    ];
    updatedElements = sortElementsByPriority(updatedElements);
    return {...logo, images: updatedImages, elements: updatedElements};
}

export const removeText = (logo, text) => {
    let updatedText = {...logo.text};
    delete updatedText[text._id];
    let updatedElements = sortElementsByPriority([
        ...logo.elements.filter(elem=>elem._id != text._id)
    ]);
    return {...logo, text: updatedText, elements: updatedElements};
}

export const removeImage = (logo, image) => {
    let updatedImages = {...logo.images};
    delete updatedImages[image._id];
    let updatedElements = sortElementsByPriority([
        ...logo.elements.filter(elem=>elem._id != image._id)
    ]);
    return {...logo, images: updatedImages, elements: updatedElements};
}

const removeTypeNamesAndId = (obj) => {
    let {__typename, _id, ...newObj} = obj;
    return newObj;
}

export const exportLogo = (logo) => {
    let {__typename, _id, id, elements, lastUpdate, ...exportedLogo} = {...logo};
    return {...exportedLogo, 
        images: Object.values(exportedLogo.images).map(img=>removeTypeNamesAndId({...img, position: removeTypeNamesAndId(img.position)})), 
        text: Object.values(exportedLogo.text).map(text=>removeTypeNamesAndId({...text, position: removeTypeNamesAndId(text.position)})), 
    };
}

export const withNewText = (logo) => {
    let highestPriority = logo.elements[logo.elements.length-1];
    if (!highestPriority)
        highestPriority=1;
    else 
        highestPriority=highestPriority.priority;
    highestPriority += 1;
    let customText = {
        ...DEFAULT_TEXT,
        priority: highestPriority,
        _id: "NEWLOGO"+highestPriority+"-"+logo.elements.length-1+uuidv4()
    };
    return withUpdatedText(logo, customText);
}

export const withNewImage = (logo) => {
    let highestPriority = logo.elements[logo.elements.length-1];
    if (!highestPriority)
        highestPriority=1;
    else 
        highestPriority=highestPriority.priority;
    highestPriority += 1;
    let customImage = {
        ...DEFAULT_IMAGE,
        priority: highestPriority,
        _id: "NEWLOGO"+highestPriority+"-"+logo.elements.length-1+uuidv4()
    };
    return withUpdatedImage(logo, customImage);
}
export const DEFAULT_LOGO = {
    borderColor: "#ffffff",
    backgroundColor: "#00ffff",
    padding: 3,
    margin: 5,
    borderRadius: 6,
    width: 500,
    height: 500,
    images: [
    ],
    text: [
    ],
    borderThickness: 1,
}

export const DEFAULT_TEXT = {
    fontSize: 12,
    color: "#000000",
    text: "Text",
    position: { posX: 0, posY: 0 },
    borderColor: "#ff0000",
    backgroundColor: "#ffffff",
    padding: 5,
    borderRadius: 5,
    borderThickness: 5,
    priority: 0,
}
export const DEFAULT_IMAGE = {
    borderColor: "#ffffff",
    scale: 100,
    borderRadius: 5,
    borderThickness: 12,
    padding: 10,
    backgroundColor: "#ffffff",
    priority: 1,
    position: {posX: 100, posY: 100},
    url:"",
}

export const FIELDS = [
    {
        label: "Border Color",
        name: "borderColor",
        type: "color"
    },
    {
        label: "Background Color",
        name: "backgroundColor",
        type: "color"
    },
    {
        label: "Border Radius",
        name: "borderRadius",
        type: "number",
        max: 500
    },
    {
        label: "Border Thickness",
        name: "borderThickness",
        type: "number",
        max: 500
    },
    {
        label: "Margin",
        name: "margin",
        type: "number"
    },
    {
        label: "Padding",
        name: "padding",
        type: "number"
    },
    {
        label: "Width",
        name: "width",
        type: "number"
    },
    {
        label: "Height",
        name: "height",
        type: "number"
    },
]

export const TEXT_FIELDS = [
    {
        label: "Text",
        name: "text",
        type: "text"
    },
    {
        label: "Border Color",
        name: "borderColor",
        type: "color"
    },
    {
        label: "Background Color",
        name: "backgroundColor",
        type: "color"
    },
    {
        label: "Border Radius",
        name: "borderRadius",
        type: "number",
        max: 500
    },
    {
        label: "Border Thickness",
        name: "borderThickness",
        type: "number",
        max: 200
    },
    {
        label: "Padding",
        name: "padding",
        type: "number"
    },
    {
        label: "Font Size",
        name: "fontSize",
        type: "number"
    },
    {
        label: "Priority",
        name: "priority",
        type: "number"
    },
]

export const IMAGE_FIELDS = [
    {
        label: "Image URL",
        name: "url",
        type: "text"
    },
    {
        label: "Border Color",
        name: "borderColor",
        type: "color"
    },
    {
        label: "Background Color",
        name: "backgroundColor",
        type: "color"
    },
    {
        label: "Border Radius",
        name: "borderRadius",
        type: "number",
        max: 200
    },
    {
        label: "Border Thickness",
        name: "borderThickness",
        type: "number",
        max: 200
    },
    {
        label: "Padding",
        name: "padding",
        type: "number"
    },
    {
        label: "Priority",
        name: "priority",
        type: "number"
    },
    {
        label: "Scale (%)",
        name: "scale",
        type: "number"
    },
]
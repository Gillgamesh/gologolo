import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import SwipeableDrawer from '@material-ui/core/SwipeableDrawer';
import {ListItemIcon, ListItemText, Divider, MenuList, MenuItem} from '@material-ui/core';
import {useQuery} from '@apollo/react-hooks';
import Typography from '@material-ui/core/Typography';
import {Link} from 'react-router-dom';
import HomeIcon from '@material-ui/icons/Home';
import AddIcon from '@material-ui/icons/Add';
import CreateIcon from '@material-ui/icons/Create';



import {VIEWER_QUERY, GET_LOGOS} from '../../queries';

const useStyles = makeStyles({
    drawer: {
      width: 250,
    },
    list: {
        width: 250,
    },
    fullList: {
      width: 'auto',
    },
    heading: {
        minHeight: 100,
    }
  });
const Drawer = (props) => {
    const muiClasses = useStyles();
    const {data, loading, error} = useQuery(VIEWER_QUERY, {pollInterval: 500});
    const logosQuery = useQuery(GET_LOGOS, {pollInterval: 500});
    const handleDrawerOpen = (event) => {
        props.setOpen(true);
    }
    const handleDrawerClose = (event) => {
        props.setOpen(false);
    }
    return (
        <SwipeableDrawer
            className={muiClasses.drawer}
            anchor="left"
            open={props.open}
            onOpen={handleDrawerOpen}
            onClose={handleDrawerClose}
            ModalProps={{keepMounted: true}}
        >
            <div
                onClick={handleDrawerClose}
                role="presentation"
                className={muiClasses.list} >
                <div className={muiClasses.heading}>
                    <br />
                    <Typography variant="h5" align='center'>
                        {loading ? 'Loading...' : (data && data.viewer && `${data.viewer.displayName}`) || 'Guest'}
                    </Typography>
                </div>
                <Divider />
                <MenuList>
                    <MenuItem component={Link} to="/" key="home">
                        <ListItemIcon>
                            <HomeIcon/>
                        </ListItemIcon>
                        <ListItemText primary={"Home"} />
                    </MenuItem>
                    <MenuItem component={Link} to="/create" key="add">
                        <ListItemIcon>
                            <AddIcon/>
                        </ListItemIcon>
                        <ListItemText primary={"Create New Logo"} />
                    </MenuItem>
                    {logosQuery.data && logosQuery.data.viewerLogos.sort((logo1, logo2) => logo1.lastUpdate == logo2.lastUpdate ? 0 : (
                        logo1.lastUpdate > logo2.lastUpdate ? -1 : 1)).map(
                            // the dates can be compared with >, regardless
                            // of whether they are ISO strings or datetime objects, the larger one
                            // will be the one with the higher date. We want the larger one to be considered "smaller" by comparator
                            (logo, index) => (
                                <MenuItem component={Link} to={`/view/${logo._id}`} key={index}>
                                    <ListItemIcon>
                                        <CreateIcon />
                                    </ListItemIcon>
                                    <ListItemText primary={
                                        (logo.text && logo.text.length) ? logo.text.map(text => (text.text + " ")) : "No Text Logo"
                                    }
                                    />
                                </MenuItem>
                            ))}
                </MenuList>
            </div>

        </SwipeableDrawer>
    )
}

export default Drawer;
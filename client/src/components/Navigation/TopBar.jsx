import React from 'react';
import { createStyles, makeStyles, Theme } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AccountCircle from '@material-ui/icons/AccountCircle';
import MenuItem from '@material-ui/core/MenuItem';
import Menu from '@material-ui/core/Menu';

import {isLoggedIn, logout} from '../../auth';
import {useHistory} from 'react-router-dom';

const useStyles = makeStyles(
    (theme) => 
    createStyles({
        menuButton: {
            marginRight: theme.spacing(2),

        },
        root: {
            flexGrow: 1,
        },
        title: {
            flexGrow: 1,
        }
    })
);

const TopBar = (props) => {
    const classes = useStyles();
    const [anchorEl, setAnchorEl] = React.useState(null);
    const history = useHistory();
    const handleMenu = (event) => {
        setAnchorEl(event.currentTarget);
    }
    const handleClose = () => {
        setAnchorEl(null);
    }

    return (
        <div className={classes.root}>
            <AppBar>
                <Toolbar>
                    <IconButton edge="start"
                        className = {classes.menuButton}
                        onClick = {props.toggleDrawer}
                        color="inherit" aria-label="Menu">
                        <MenuIcon />
                    </IconButton>
                    <Typography variant="h6" className={classes.title}>
                        GoLogoLo 
                    </Typography>
                        {(
                            <div>
                                <IconButton
                                    aria-label="Account of current user"
                                    aria-controls="menu-appbar"
                                    aria-haspopup="true"
                                    onClick={handleMenu}
                                    color="inherit"
                                >
                                    <AccountCircle />
                                </IconButton>
                                <Menu
                                    id="menu-appbar"
                                    anchorEl={anchorEl}
                                    anchorOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    keepMounted
                                    transformOrigin={{
                                        vertical: 'top',
                                        horizontal: 'right',
                                    }}
                                    open={Boolean(anchorEl)}
                                    onClose={handleClose}
                                >
                                <MenuItem onClick={() => {
                                    logout().then(() => {
                                        setAnchorEl(null);
                                        history.push('/');
                                    });
                                    }
                                        }>Log Out</MenuItem>
                                </Menu>
                            </div>
                        )}


                </Toolbar>
            </AppBar>
        </div>
    )
}

export default TopBar;
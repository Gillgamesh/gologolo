import React from 'react';
import TopBar from './TopBar';
import Drawer from './Drawer';


const Navigation = (props) => {
    const [drawerOpen, setDrawerOpen] = React.useState(false);
    return (
        <React.Fragment>
            <TopBar
                toggleDrawer={() => setDrawerOpen(!drawerOpen)}
            />
            <Drawer
                open={drawerOpen}
                setOpen={setDrawerOpen}
            />
        </React.Fragment>
    )
}


export default Navigation;
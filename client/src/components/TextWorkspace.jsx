import React, { Component, Fragment } from 'react';
import Draggable from 'react-draggable';

// THIS IS HOW WE DISPLAY THE LOGO TEXT , IN THIS COMPONENT
const TextElement = (props) => {
    const styles = {
        textPiece: {
            position: "absolute",
            color: props.text.color,
            fontSize: props.text.fontSize + "pt",
            padding: props.text.padding + "px",
            backgroundColor: props.text.backgroundColor,
            borderStyle: "solid",
            borderColor: props.text.borderColor,
            borderRadius: props.text.borderRadius + "px",
            borderWidth: props.text.borderThickness + "px",
            // marginLeft: props.text.position.posX + "px",
            // marginTop: props.text.position.posY + "px",
            cursor: props.editable ? "move" : "default"
        }
    }

    const updatePosition = (event, data) => {
        let updatedElement = {
            ...props.text,
            position: {
                posX: data.x,
                posY: data.y
            }
        }
        props.updateText(updatedElement);
    }
    return (
        <Draggable
            disabled={!props.editable}
            position={{
                x: props.text.position.posX,
                y: props.text.position.posY,
            }}
            onStop={updatePosition}
        >
            <div
                style={styles.textPiece}>
                <span>{props.text.text.replace(/ /g, '\u00a0')}
                </span>
            </div>
        </Draggable>
    )
}

const ImageElement = (props) => {
    const styles = {
        imagePiece: {
            position: "absolute",
            padding: props.img.padding + "px",
            backgroundColor: props.img.backgroundColor,
            borderStyle: "solid",
            borderColor: props.img.borderColor,
            borderRadius: props.img.borderRadius + "px",
            borderWidth: props.img.borderThickness + "px",
            width: Math.round(props.img.scale) + "%",
            cursor: props.editable ? "move" : "default"
        }
    }
    const updatePosition = (event, data) => {
        let updatedElement = {
            ...props.img,
            position: {
                posX: data.x,
                posY: data.y
            }
        }
        props.updateImage(updatedElement);
    }
    // prevent html drag:
    return (
        <Draggable
            disabled={!props.editable}
            position={{
                x: props.img.position.posX,
                y: props.img.position.posY,
            }}
            onStop={updatePosition}
        >
            <div
                style={styles.imagePiece}>
                <img
                    src={props.img.url}
                    width="100%"
                    height="auto"
                    draggable="false"
                />
            </div>
        </Draggable>
    )
}

class TextEditWorkspace extends Component {
    render() {
        const styles = {
            container: {
                // position: "relative",
                backgroundColor: this.props.logo.backgroundColor,
                borderStyle: "solid",
                borderColor: this.props.logo.borderColor,
                borderRadius: this.props.logo.borderRadius + "px",
                borderWidth: this.props.logo.borderThickness + "px",
                margin: this.props.logo.margin + "px",
                padding: this.props.logo.padding + "px",
                width: this.props.logo.width + "px",
                height: this.props.logo.height+"px",
            }
        }
        //we can abuse the fact that this.props.logo is updated by text
        return (
            <div
                style={styles.container}>
                <React.Fragment>
                    {this.props.logo.elements.map((element) => (
                        element.type === "text" ?
                            (
                                <TextElement
                                    text={this.props.logo.text[element._id]}
                                    updateText={(updated) => this.props.updateText(updated)}
                                    editable={this.props.editable}
                                    key={element._id}
                                />
                            ) : (
                                <ImageElement
                                    img={this.props.logo.images[element._id]}
                                    updateImage={(updated) => this.props.updateImage(updated)}
                                    key={element._id}
                                    editable={this.props.editable}
                                />
                            )
                    ))}
                </React.Fragment>
            </div>
        )
    }
}

export default TextEditWorkspace
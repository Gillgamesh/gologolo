import React, { Component} from 'react';
import TextWorkspace from './TextWorkspace';
import {
    withUpdatedText, withUpdatedImage, 
    removeText, removeImage,
    withNewText, withNewImage
} from '../serializers';
import LayersEditor from './LayersEditor';

import Button from '@material-ui/core/Button';
import SaveIcon from '@material-ui/icons/Save';
import TextFieldsIcon from '@material-ui/icons/TextFields';
import ImageIcon from '@material-ui/icons/Image';


class LogoEditor extends Component {
    constructor(props) {
        super(props);
        this.state = {
            logo: this.props.logo
        };
    }

    deleteText = (text) => {
        this.setState({
            ...this.state,
            logo: removeText(this.state.logo, text)
        })
    }
    deleteImage = (image) => {
        this.setState({
            ...this.state,
            logo: removeImage(this.state.logo, image)
        })
    }

    addText = () => {
        this.setState({
            ...this.state,
            logo: withNewText(this.state.logo)
        })
    }
    addImage = () => {
        this.setState({
            ...this.state,
            logo: withNewImage(this.state.logo)
        })
    }

    changeText = (updatedText) => {
        this.setState({
            ...this.state,
            logo: withUpdatedText(this.state.logo, updatedText)
        })
    }
    changeImage = (updatedImage) => {
        this.setState({
            ...this.state,
            logo: withUpdatedImage(this.state.logo, updatedImage)
        })
    }
    changeProp = (propName, newValue) => {
        let newLogo = {...this.state.logo};
        newLogo[propName] = newValue;
        this.setState({
            ...this.state, 
            logo: newLogo
        });
    }
    handleNumericPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            propValue = parseInt(propValue);
            this.changeProp(propName, propValue);
        }
    )
    handleStringPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            this.changeProp(propName, propValue)
        }
    )
    render() {
        return (
            <div className="row">
                <div className="col-4">
                    <div className="container">
                        <div className="row" style={{marginTop: "10px", marginBottom: "10px"}}>
                            <div className="col-6">
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                startIcon={<TextFieldsIcon />}
                                onClick={this.addText}
                            >
                                New Text
                            </Button>
                            </div>
                            <div className="col-6">
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                startIcon={<ImageIcon />}
                                onClick={this.addImage}
                            >
                                New Image
                            </Button>
                            </div>
                        </div>
                        <div className="row">
                            <LayersEditor
                                logo={this.state.logo}
                                handleStringPropChange={this.handleStringPropChange}
                                handleNumericPropChange={this.handleNumericPropChange}
                                changeProp={this.changeProp}
                                updateText={this.changeText}
                                updateImage={this.changeImage}
                                deleteText={this.deleteText}
                                deleteImage={this.deleteImage}
                             />
                        </div>
                        <div className="row" style={{marginTop: "20px"}}>
                            <Button
                                variant="contained"
                                color="primary"
                                size="large"
                                startIcon={<SaveIcon />}
                                onClick={() => this.props.submit(this.state.logo)}
                            >
                                Save And Exit
                            </Button>
                        </div>
                    </div>
                </div>
                <div className="col s8">
                    <TextWorkspace
                        logo={this.state.logo}
                        updateText={this.changeText}
                        updateImage={this.changeImage}
                        editable={true}
                    />
                </div>
            </div>
        );
    }
}


export default LogoEditor;
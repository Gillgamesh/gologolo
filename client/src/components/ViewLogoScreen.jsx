import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import { Query, Mutation } from 'react-apollo';
import {GET_LOGO, GET_LOGOS, DELETE_LOGO} from '../queries';
import {DEFAULT_LOGO, FIELDS} from '../constants';

import TextWorkspace from './TextWorkspace';
import {serializeLogo} from '../serializers';

import Button from '@material-ui/core/Button';
import ImageIcon from '@material-ui/icons/Image';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { CssBaseline } from '@material-ui/core';
import htmlToImage from 'html-to-image';
import { saveAs } from 'file-saver';




class ViewLogoScreen extends Component {
    constructor(props) {
        super(props);
        this.domRef = React.createRef();
    }

    handleDownload = () => {
        let node = this.domRef.current;
        console.log(node);
        htmlToImage.toPng(node).then((blob) => {
            window.saveAs(blob, 'gologo.png')
        }
        );
    }
    render() {
        return (
            <Query pollInterval={500} query={GET_LOGO} variables={{ id: this.props.match.params.id }}>
                {({ loading, error, data }) => {
                    if (loading) return 'Loading...';
                    if (error) return `Error! ${error.message}`;

                    return (
                        <div className="container">
                            <div className="panel panel-default">
                                <div className="panel-heading">
                                    <h3 className="panel-title">
                                        View Logo
                                    </h3>
                                </div>
                                <div className="panel-body">
                                    <div className="container">
                                        <div className="row">
                                            <div className="col-4">
                                                <dl>
                                                </dl>
                                                <Mutation
                                                    mutation={DELETE_LOGO}
                                                    key={data.logo._id}
                                                    onCompleted={() => this.props.history.push('/')}
                                                    refetchQueries={() =>
                                                        [
                                                            { query: GET_LOGOS },
                                                        ]
                                                    }
                                                >
                                                    {(removeLogo, { loading, error }) => (
                                                        <div>
                                                            <CssBaseline/>
                                                            <Button
                                                                variant="contained"
                                                                onClick={this.handleDownload}
                                                                startIcon={<ImageIcon />}
                                                            >Download</Button>
                                                            <Button
                                                                variant="contained"
                                                                color="primary"
                                                                component={Link}
                                                                to={`/edit/${data.logo.id}`}
                                                                startIcon={<EditIcon />}
                                                            >Edit</Button>
                                                            <Button
                                                                variant="contained"
                                                                color="secondary"
                                                                startIcon={<DeleteIcon />}
                                                                onClick={()=>{
                                                                    removeLogo({ variables: { id: data.logo._id } });
                                                                }}
                                                            >Delete</Button>
                                                            {loading && <p>Loading...</p>}
                                                            {error && <p>Error :( Please try again</p>}
                                                        </div>
                                                    )}
                                                </Mutation>
                                            </div>
                                            <div
                                                className="col-8"
                                                ref={this.domRef}
                                            >
                                                <TextWorkspace
                                                    logo={serializeLogo(data.logo)}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    );
                }}
            </Query>
        );
    }
}

export default ViewLogoScreen;
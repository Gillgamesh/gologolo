import React from 'react';
import { FIELDS, TEXT_FIELDS, IMAGE_FIELDS, DEFAULT_LOGO } from '../constants';
import { makeStyles } from '@material-ui/core/styles';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import Typography from '@material-ui/core/Typography';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import DeleteIcon from '@material-ui/icons/Delete';


const useStyles = makeStyles((theme) => ({
    root: {
        width: '100%',
    },
    heading: {
        fontSize: theme.typography.pxToRem(15),
        flexBasis: '80%',
        flexShrink: 0,
    },
    secondaryHeading: {
        fontSize: theme.typography.pxToRem(15),
        color: theme.palette.text.secondary,
    },
}));

const Input = (props) => {
    // defaultValue
    // value
    // label
    // min - max
    return props.type!=="color" ? (
        <React.Fragment>
            <div className="row">
                <label> {props.label} </label>
            </div>
            <div className="row" style={{ marginBottom: '5px' }}>
                <TextField
                    type={props.type}
                    value={props.value}
                    min={(!!props.min && props.type === "number" && props.min) || undefined}
                    max={(!!props.max && props.type === "number" && props.max) || undefined}
                    onChange={(event) => {
                        if (props.type !== "number")
                            return props.onChange(event);
                        let value = parseInt(event.target.value);
                        if ((!props.min || props.min <= value)
                            && (0 <= value)
                            && (!props.max || props.max >= value)
                        )
                            props.onChange(event);
                    }
                    }
                />
            </div>
        </React.Fragment>
    ) : (
        <React.Fragment>
            <div className="row">
                <label> {props.label} </label>
            </div>
            <div className="row" style={{ marginBottom: '5px' }}>
                <input
                    type={props.type}
                    value={props.value}
                    min={(!!props.min && props.type === "number" && props.min) || undefined}
                    max={(!!props.max && props.type === "number" && props.max) || undefined}
                    onChange={(event) => {
                        if (props.type !== "number")
                            return props.onChange(event);
                        let value = parseInt(event.target.value);
                        if ((!props.min || props.min <= value)
                            && (0 <= value)
                            && (!props.max || props.max >= value)
                        )
                            props.onChange(event);
                    }
                    }
                />
            </div>
        </React.Fragment>
    )
};


const TextLayer = (props) => {
    let fields = TEXT_FIELDS;
    const changeProp = (propName, newValue) => {
        let updatedText = { ...props.text };
        updatedText[propName] = newValue;
        props.updateText(updatedText);
    };
    const handleNumericPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            propValue = parseInt(propValue);
            changeProp(propName, propValue);
        }
    );
    const handleStringPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            changeProp(propName, propValue)
        }
    );
    const handleDelete = () => {
        props.deleteText(props.text);
    };
    return (
        <React.Fragment>
            {fields.map(logoProperty => {
                return logoProperty.type !== "number" ? (
                    <Input
                        value={props.text[logoProperty.name]}
                        onChange={handleStringPropChange(logoProperty.name)}
                        label={logoProperty.label}
                        type={logoProperty.type}
                        key={logoProperty.name}
                    />
                ) : (
                        <Input
                            value={props.text[logoProperty.name]}
                            onChange={handleNumericPropChange(logoProperty.name)}
                            type={logoProperty.type}
                            label={logoProperty.label}
                            key={logoProperty.name}
                            min={logoProperty.min ? logoProperty.min : undefined}
                            max={logoProperty.max ? logoProperty.max : undefined}
                        />
                    )
            })}

            <div className="row" style={{ marginBottom: "10px" }}>
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<DeleteIcon />}
                    onClick={handleDelete}
                >
                    Delete
                </Button>
            </div>
        </React.Fragment>

    )
};

const ImageLayer = (props) => {
    let fields = IMAGE_FIELDS;
    const changeProp = (propName, newValue) => {
        let updatedImage = { ...props.image };
        updatedImage[propName] = newValue;
        props.updateImage(updatedImage);
    };
    const handleNumericPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            propValue = parseInt(propValue);
            changeProp(propName, propValue);
        }
    );
    const handleStringPropChange = (propName) => (
        (event) => {
            let propValue = event.target.value;
            changeProp(propName, propValue)
        }
    );
    const handleDelete = () => {
        props.deleteImage(props.image);
    };
    return (
        <React.Fragment>
            {fields.map(logoProperty => {
                return logoProperty.type !== "number" ? (
                    <Input
                        value={props.image[logoProperty.name]}
                        onChange={handleStringPropChange(logoProperty.name)}
                        label={logoProperty.label}
                        type={logoProperty.type}
                        key={logoProperty.name}
                    />
                ) : (
                        <Input
                            value={props.image[logoProperty.name]}
                            onChange={handleNumericPropChange(logoProperty.name)}
                            type={logoProperty.type}
                            label={logoProperty.label}
                            key={logoProperty.name}
                            min={logoProperty.min ? logoProperty.min : undefined}
                            max={logoProperty.max ? logoProperty.max : undefined}
                        />
                    )
            })}
            <div className="row" style={{ marginBottom: "10px" }}>
                <Button
                    variant="contained"
                    color="secondary"
                    startIcon={<DeleteIcon />}
                    onClick={handleDelete}
                >
                    Delete
                </Button>
            </div>
        </React.Fragment>

    )
};

const LayersEditor = (props) => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState("general");

    const setActivePanel = (panel) => (event, isExpanded) => {
        setExpanded(isExpanded ? panel : false);
    };

    let fields = FIELDS;
    return (
        <div className={classes.root}>

            <ExpansionPanel expanded={expanded === "general"} onChange={setActivePanel("general")}>
                <ExpansionPanelSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls="panel1bh-content"
                    id="panel1bh-header"
                >
                    <Typography className={classes.heading}>General Settings</Typography>
                </ExpansionPanelSummary>
                <ExpansionPanelDetails>
                    <div className="container">
                        {
                            fields.map(logoProperty => {
                                return logoProperty.type !== "number" ? (
                                    <Input
                                        value={props.logo[logoProperty.name]}
                                        onChange={props.handleStringPropChange(logoProperty.name)}
                                        label={logoProperty.label}
                                        type={logoProperty.type}
                                        key={logoProperty.name}
                                    />
                                ) : (
                                        <Input
                                            value={props.logo[logoProperty.name]}
                                            onChange={props.handleNumericPropChange(logoProperty.name)}
                                            type={logoProperty.type}
                                            label={logoProperty.label}
                                            key={logoProperty.name}
                                            min={logoProperty.min ? logoProperty.min : undefined}
                                            max={logoProperty.max ? logoProperty.max : undefined}
                                        />
                                    )
                            })

                        }
                        <div className="row" style={{ marginBottom: "10px" }}>
                        </div>
                    </div>
                </ExpansionPanelDetails>
            </ExpansionPanel>
            {props.logo.elements.slice().reverse().map((element, index) => {
                return element.type=="text" ? (
                    <ExpansionPanel key={element._id} expanded={expanded == element._id} onChange={setActivePanel(element._id)}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                        >
                            <Typography className={classes.heading}>{props.logo.text[element._id].text}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="container">
                            <TextLayer
                                text={props.logo.text[element._id]}
                                updateText={props.updateText}
                                deleteText={props.deleteText}
                            />
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                ) : (
                    <ExpansionPanel key={element._id} expanded={expanded == element._id} onChange={setActivePanel(element._id)}>
                        <ExpansionPanelSummary
                            expandIcon={<ExpandMoreIcon />}
                            aria-controls="panel1bh-content"
                            id="panel1bh-header"
                        >
                            <Typography className={classes.heading}>{`Image Layer (#${index+1})`}</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <div className="container">
                            <ImageLayer
                                image={props.logo.images[element._id]}
                                updateImage={props.updateImage}
                                deleteImage={props.deleteImage}
                            />
                            </div>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                )
            })}
        </div>
    );
}
export default LayersEditor;
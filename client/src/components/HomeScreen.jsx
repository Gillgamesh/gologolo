import React from 'react';
import { Link } from 'react-router-dom';
import '../App.css';
import { Query } from 'react-apollo';
import { GET_LOGOS } from '../queries';

import ImageIcon from '@material-ui/icons/Image';
import Button from '@material-ui/core/Button';

/*
Switching to class components, because functional hooks are less ugly than withX decorators
 */
const HomeScreen = () => {
    return (
        <Query pollInterval={500} query={GET_LOGOS}>
            {({ loading, error, data }) => {
                if (loading || error) return 'Loading...';
                return (
                    <div className="container" style={{ marginTop: "50px" }}>
                        <div className="row">
                            <div className="col s4">
                                <h3>Recent Work</h3>
                                {data.viewerLogos.sort((logo1, logo2) => logo1.lastUpdate === logo2.lastUpdate ? 0 : (
                                    logo1.lastUpdate > logo2.lastUpdate ? -1 : 1)).map(
                                        // the dates can be compared with >, regardless
                                        // of whether they are ISO strings or datetime objects, the larger one
                                        // will be the one with the higher date. We want the larger one to be considered "smaller" by comparator
                                        (logo, index) => (
                                            <div key={index} className='home_logo_link'
                                                style={{ cursor: "pointer" }}>
                                                <Link to={`/view/${logo._id}`}>
                                                    {(logo.text && logo.text.length) ? logo.text.map(text => (text.text + " ")) : "No Text Logo"}
                                                </Link>
                                            </div>
                                        ))}
                            </div>
                            <div className="col s8">
                                <div id="home_banner_container">
                                    GoLogoLo<br />
                                    Logo Maker
                                </div>
                                <div>
                                    <br />
                                    <Button
                                        variant="contained"
                                        color="primary"
                                        component={Link}
                                        to={`/create`}
                                        startIcon={<ImageIcon />}
                                    >Create a New Logo</Button>
                                </div>
                            </div>
                        </div>
                    </div>
                );
            }
            }
        </Query >
    );
}

export default HomeScreen;

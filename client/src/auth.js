const jwt = require('jsonwebtoken');

export function getLocalAccessToken() {
    return sessionStorage.getItem('accessToken');
}

export function isTokenValid(token) {
    let decoded = jwt.decode(token);
    if (!decoded)
        return false;
    let expirationTime = decoded["iat"];
    if (!expirationTime) return false;
    return expirationTime*1000 > Date.now();
}

export function isLoggedIn() {
    return !!getLocalAccessToken();
}

export function logout() {
    return fetch(`${process.env.REACT_APP_GOLO_SERVER_ADDR}/auth/clearSession`, { credentials: 'include' }).then(
        (res) => {
            sessionStorage.clear();
        }
    )
}
